TODOPLUS List App
G1T4 

Aw Jie Shi
Ian Chow Li Ren
Joshua Huang Weijie
Tee Shi Hui



Features:
1. Adding a new Task
	- tap “+” button
	- enter task name
	- tap “Set Reminder“
	- tap Date and Time to set date time (Reminds you every 1 minute)
	- tap Location to set Location preference
	- Tap Weather to set Weather preference

2. Editing a Task
	- tap title of task to edit
	- enter new task name
	- tap "confirm" button"

3. Deleting a Task
	- on main screen
	- tap "✓" button on the right side

4. Marking a Task as Complete
	- on main screen
	- tap "✓" button on right side




