package sg.edu.smu.is416.todoplus;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import sg.edu.smu.is416.todoplus.db.TaskContract;
import sg.edu.smu.is416.todoplus.db.TaskDbHelper;

public class DetailsActivity extends AppCompatActivity {
    private static int REQ_CODE_LOCATION_PICKER = 5566;

    private boolean useDateTime = false;
    private boolean useLocation = false;
    private boolean useWeather = false;

    String taskName = "";

    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int seconds;

    Button locationButton;

    List<Map<String, String>> placesList;
    /*
    Map: (
        name -> location name,
        latitude -> latitude coordinate,
        longitude -> longitude coordinate
    )
    */

    public static TaskDbHelper mHelper;

    /** ========================= show hide methods ========================= */

    public void showHideDateTime(View view) {
        View viewDateTime = findViewById(R.id.layoutDateTime);
        if (!useDateTime) {
            viewDateTime.setVisibility(View.VISIBLE);
        } else {
            viewDateTime.setVisibility(View.GONE);
        }
        useDateTime = !useDateTime;
    }
    public void showHideLocation(View view) {
        View viewLocations = findViewById(R.id.layoutLocations);
        if (!useLocation) {
            viewLocations.setVisibility(View.VISIBLE);
        } else {
            viewLocations.setVisibility(View.GONE);
        }
        useLocation = !useLocation;
    }
    public void showHideWeather(View view) {
        View viewWeather = findViewById(R.id.weatherSelection);
        if (!useWeather) {
            viewWeather.setVisibility(View.VISIBLE);
        } else {
            viewWeather.setVisibility(View.GONE);
        }
        useWeather = !useWeather;
    }


    /** ========================= onCreate method ========================= */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        View viewDateTime = findViewById(R.id.layoutDateTime);
//        viewDateTime.setVisibility(View.GONE);
//        View viewLocations = findViewById(R.id.layoutLocations);
//        viewLocations.setVisibility(View.GONE);
//        View viewWeather = findViewById(R.id.weatherSelection);
//        viewWeather.setVisibility(View.GONE);

        placesList = new ArrayList<>();
        mHelper = new TaskDbHelper(this);


        String functionName = "";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            taskName = extras.getString("taskName");
            functionName = extras.getString("function");
        }

        setContentView(R.layout.activity_details);
        TextView textView = (TextView) findViewById(R.id.taskNameTV);
        textView.setText(taskName);


        // weather stuff
        Spinner dropdown = findViewById(R.id.weatherSelection);
        String[] weather_items = new String[]{
                "Clear",
                "Cloudy",
                "Rainy"
        };
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, weather_items);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.my_spinner_style,weather_items);
        dropdown.setAdapter(adapter);

        locationButton = (Button) findViewById(R.id.location_button);
        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsActivity.this, LocationPicker.class);
                startActivityForResult(intent, REQ_CODE_LOCATION_PICKER);
            }
        });

        Calendar currCalendar = Calendar.getInstance();
        currCalendar.setTimeZone(TimeZone.getTimeZone("Asia/Hong_Kong"));

        year = currCalendar.get(Calendar.YEAR);
        month = currCalendar.get(Calendar.MONTH);
        day = currCalendar.get(Calendar.DAY_OF_MONTH);
        hour = currCalendar.get(Calendar.HOUR_OF_DAY);
        minute = currCalendar.get(Calendar.MINUTE);
        seconds = 0;

        if (functionName.equals("add")) {
            if (placesList.size() > 0) {
                for (int i = 0; i < placesList.size(); i++) {
                    Map location = placesList.get(i);
                }
            }
            displaySelectedLocations();
            dropdown.setSelection(0);

        } else if (functionName.equals("edit")) {
            // retrieve task details from database

            SQLiteDatabase db = mHelper.getReadableDatabase();

            String whereClause = "title LIKE \"" + taskName + "\"";
            Cursor cursor = db.query(TaskContract.TaskEntry.TABLE,
                    null, whereClause, null, null, null, null); // get all columns

            String dateTime = "";
            String location = "";
            String weather = "";

            while (cursor.moveToNext()) {
                dateTime = cursor.getString(cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_TIME));
                location = cursor.getString(cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_LOCATION));
                weather = cursor.getString(cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_WEATHER));
            }


            // set date time variables
            if (dateTime != null && !dateTime.equals("")) {
                useDateTime = true;
                View viewDateTime = findViewById(R.id.layoutDateTime);
                viewDateTime.setVisibility(View.VISIBLE);
                Switch remindDateTime = (Switch) findViewById(R.id.remindDateTime);
                remindDateTime.setChecked(true);

                String[] dateTimeArr = dateTime.split(" ");
                String[] dateArr = dateTimeArr[0].split("-");
                String[] timeArr = dateTimeArr[1].split(":");

                year = Integer.parseInt(dateArr[0]);
                month = Integer.parseInt(dateArr[1]);
                day = Integer.parseInt(dateArr[2]);
                hour = Integer.parseInt(timeArr[0]);
                minute = Integer.parseInt(timeArr[1]);
                seconds = Integer.parseInt(timeArr[2]);

                // set textview display
                TextView textViewDate = (TextView) findViewById(R.id.in_date);
                textViewDate.setText(day + "-" + month + "-" + year);

                TextView textViewTime = (TextView) findViewById(R.id.in_time);
                String time = "" + (hour <= 12 ? hour : hour - 12) + ":" + (minute < 10 ? "0" + minute : minute) + (hour < 12 ? " am" : " pm");
                textViewTime.setText(time);
            }

            if (location != null && !location.equals("")) {
                useLocation = true;
                View viewLocations = findViewById(R.id.layoutLocations);
                viewLocations.setVisibility(View.VISIBLE);
                Switch remindLocations = (Switch) findViewById(R.id.remindLocation);
                remindLocations.setChecked(true);

                // populate placesList
                String[] locations = location.split("''");
                for (int i = 0;  i < locations.length; i++) {
                    String locationStr = locations[i];
                    if (locationStr.length() > 0) {
                        String[] locationArr = locationStr.split(",,");

                        HashMap place = new HashMap();
                        place.put("name", locationArr[0]);
                        place.put("latitude", locationArr[1]);
                        place.put("longitude", locationArr[2]);

                        placesList.add(place);

                    }
                }
            }

            if (weather != null && !weather.equals("")) {
                useWeather = true;
                View viewWeather = findViewById(R.id.weatherSelection);
                viewWeather.setVisibility(View.VISIBLE);
                Switch remindWeather = (Switch) findViewById(R.id.remindWeather);
                remindWeather.setChecked(true);

                int weatherInt = Arrays.asList(weather_items).indexOf(weather);
                dropdown.setSelection(weatherInt);
            }

        }

        displaySelectedLocations();

    }

    private void displaySelectedLocations() {
        ((TextView) findViewById(R.id.location1)).setText("-");
        ((TextView) findViewById(R.id.location2)).setText("-");
        ((TextView) findViewById(R.id.location3)).setText("-");

        if (placesList.size() > 0) ((TextView) findViewById(R.id.location1)).setText(placesList.get(0).get("name"));
        if (placesList.size() > 1) ((TextView) findViewById(R.id.location2)).setText(placesList.get(1).get("name"));
        if (placesList.size() > 2) ((TextView) findViewById(R.id.location3)).setText(placesList.get(2).get("name"));

    }


    /** ========================= onClick methods ========================= */

    public void changeTaskName(View view) {
        RelativeLayout vwParentRelative = (RelativeLayout) view.getParent();
        LinearLayout vwParentLinear = (LinearLayout) vwParentRelative.getChildAt(0);
        String taskToEdit = ((TextView) vwParentLinear.getChildAt(0)).getText().toString();

        final EditText taskEditText = new EditText(this);
        taskEditText.setText(taskToEdit);

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Edit task name")
                .setView(taskEditText)
                .setPositiveButton("confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                    String temp = taskName.toLowerCase().trim();
//                    if (temp.equals("location") || temp.equals("weather") || temp.equals("time")) {
                        //remove old task
                        removeTask(taskName);

                        taskName = String.valueOf(taskEditText.getText()).replaceAll("[^a-zA-Z0-9 ]","");
                        ((TextView) findViewById(R.id.taskNameTV)).setText(taskName);


//                    } else {
//                        Toast.makeText(getApplicationContext(), "error: key word not allowed", Toast.LENGTH_SHORT).show();
//                    }
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    public void pickTime(View view) {
        final TextView editTextView = (TextView) view;

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minuteInput) {
                hour = hourOfDay;
                minute = minuteInput;

                String time = ""+(hour<=12 ? hour : hour-12)+":"+(minute<10 ? "0"+minute : minute)+(hour<12 ? " am" : " pm");
                editTextView.setText(time);
            }
        }, hour, minute, false);
        timePickerDialog.show();
    }

    public void pickDate(View view) {
        final TextView editTextView = (TextView) view;

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int yearInput, int monthOfYear, int dayOfMonth) {
                year = yearInput;
                month = monthOfYear + 1;
                day = dayOfMonth;

                editTextView.setText(day + "-" + month + "-" + year);
            }
        }, year, month, day);
        datePickerDialog.show();

    }

    public void submitTask(View view) {
        // get task name
        TextView taskNameTV = (TextView) findViewById(R.id.taskNameTV);
        String taskName = (String) taskNameTV.getText();


        String dateTime = "";
        String location = "";
        String weather = "";

        // get dateTime
        if (useDateTime) {
            String time = "";
            time += (day<10 ? "0"+day : day) + "-" + (month<10 ? "0"+month : month) + "-" + year + " ";
            time += (hour<10 ? "0"+hour : hour)+":"+(minute<10 ? "0"+minute : minute)+":"+(seconds<10 ? "0"+seconds : seconds);

            dateTime = time;
        }

        // get location
        if (useLocation) {
            location = "";
            for (int i = 0; i < placesList.size(); i++) {
                Map place = placesList.get(i);
                String locationName = (String) place.get("name");
                String locationLat = (String) place.get("latitude");
                String locationLng = (String) place.get("longitude");

                location += locationName + ",," + locationLat + ",," + locationLng;
                if (i + 1 != placesList.size()) {
                    location += "''";
                }
            }
        }

        // get weather
        if (useWeather) {
            Spinner weatherSpinner = (Spinner) findViewById(R.id.weatherSelection);
            weather = weatherSpinner.getSelectedItem().toString();

        }

        // add task
        addTask(taskName, dateTime, location, weather);

        // go back to home
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    public void addTask(String taskName, String dateTime, String location, String weather) {
        SQLiteDatabase db = mHelper.getWritableDatabase();

        removeTask(taskName, db);

        // add new one
        ContentValues values = new ContentValues();
        values.put(TaskContract.TaskEntry.COL_TASK_TITLE, taskName);
        values.put(TaskContract.TaskEntry.COL_TASK_TIME, dateTime);
        values.put(TaskContract.TaskEntry.COL_TASK_LOCATION, location);
        values.put(TaskContract.TaskEntry.COL_TASK_WEATHER, weather);
        db.insertWithOnConflict(TaskContract.TaskEntry.TABLE,
                null,
                values,
                SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public void removeTask(String taskName, SQLiteDatabase db) {
        // remove old task (if it exists)
        String whereClause = TaskContract.TaskEntry.COL_TASK_TITLE + " = \"" + taskName + "\"";
        db.delete(TaskContract.TaskEntry.TABLE, whereClause, null);
    }

    public void removeTask(String taskName) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        removeTask(taskName, db);
        db.close();
    }


    /** ========================= intent activity methods ========================= */

    // select on map result
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_CODE_LOCATION_PICKER) {
            if (resultCode == RESULT_OK) {
                Bundle extrasLoc = data.getExtras();
                if(extrasLoc != null) {
                    String[] locationName = extrasLoc.getString("locationNames").split(",");
                    String[] locationLats = extrasLoc.getString("locationLats").split(",");
                    String[] locationLngs = extrasLoc.getString("locationLngs").split(",");

                    placesList = new ArrayList<>();
                    for (int i = 0; i < locationName.length; i++) {
                        HashMap place = new HashMap();
                        place.put("name", locationName[i]);
                        place.put("latitude", locationLats[i]);
                        place.put("longitude", locationLngs[i]);

                        placesList.add(place);
                    }

                    displaySelectedLocations();

                }

            }
        }
    }

}


