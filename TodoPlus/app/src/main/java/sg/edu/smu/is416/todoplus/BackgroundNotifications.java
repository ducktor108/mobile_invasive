package sg.edu.smu.is416.todoplus;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import sg.edu.smu.is416.todoplus.db.TaskContract;
import sg.edu.smu.is416.todoplus.db.TaskDbHelper;

public class BackgroundNotifications extends Service {
    private static final String GROUP_KEY_TODOPLUS = "sg.edu.smu.is416.todoplus";

    private static final String TAG = "MyLocationService";
    private LocationManager mLocationManager = null;
    BackgroundNotifications.LocationListener[] mLocationListeners = null;
    private static final int INTERVAL = 1 * 60 * 1000; // in milliseconds
    private static final float LOCATION_DISTANCE = 500; // in meters
    private static Location currentLocation;

    private static String weather_description_high;

    public static NotificationManager notificationManager;
    public static Notification.Builder noti;
    private static String notiTitle;
    private static String notiText;

    private Timer timer = new Timer();

    public static TaskDbHelper mHelper;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /** ========================= onCreate method ========================= */

    @Override
    public void onCreate() {
        super.onCreate();

        mHelper = new TaskDbHelper(this);
        weather_description_high = "";

//        inboxStyle = new Notification.InboxStyle();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        noti = new Notification.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.ic_action_name)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_foreground))
                .setVibrate(new long[] { 500, 500 })
                .setLights(Color.BLUE, 3000, 3000)
                .setAutoCancel(true)
                .setGroup(GROUP_KEY_TODOPLUS)
                .setPriority(Notification.PRIORITY_MAX);


        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                initializeLocationManager();

                // get current location
                Location location = currentLocation;

//                try {
                    SQLiteDatabase db = mHelper.getReadableDatabase();

                    Cursor cursor = db.query(TaskContract.TaskEntry.TABLE,
                            null, null, null, null, null, null);

                    List<Task> tasks = new ArrayList<>();
                    while (cursor.moveToNext()) {
                        String taskTitle = cursor.getString(cursor.getColumnIndexOrThrow(TaskContract.TaskEntry.COL_TASK_TITLE));
                        String taskTime = cursor.getString(cursor.getColumnIndexOrThrow(TaskContract.TaskEntry.COL_TASK_TIME));
                        Date taskDate = null;
                        try {
                            if (taskTime!=null && taskTime.length() > 0) {
                                taskDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(taskTime);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        String taskWeather = cursor.getString(cursor.getColumnIndexOrThrow(TaskContract.TaskEntry.COL_TASK_WEATHER));
                        String taskLocation = cursor.getString(cursor.getColumnIndexOrThrow(TaskContract.TaskEntry.COL_TASK_LOCATION));

                        Task task = new Task(taskTitle, taskDate, taskLocation, taskWeather);
                        tasks.add(task);
                    }
                    cursor.close();

                    // check weather

                    WeatherAPIFunction.placeIdTask asyncTask = new WeatherAPIFunction.placeIdTask(new WeatherAPIFunction.AsyncResponse() {
                        public void processFinish(String weather_city, String weather_description, String weather_temperature, String weather_humidity, String weather_pressure, String weather_updatedOn, String weather_iconText, String sun_rise) {
                            weather_description_high = weather_description.toLowerCase();

                        }
                    });
                    asyncTask.execute(Double.toString(currentLocation.getLatitude()), Double.toString(currentLocation.getLongitude()));

                    // loop tasks
                    outer:
                    for (int i = 0; i < tasks.size(); i++) {
                        Task task = tasks.get(i);

                        notiTitle = task.getName();
                        notiText = "";

                        if (notiTitle.equals("datetime")) {
                            Log.e(TAG, task.getTime().toString());
                        }

                        // check date time
                        if (task.getTime() != null) {
                            Date taskDate = task.getTime();
                            Date currentDate = new Date();
                            Date taskDate30 = new Date(taskDate.getTime() - 30 * 60 * 1000);

                            // if date is within 0.5 hr of task's date
                            if (currentDate.before(taskDate) && currentDate.after(taskDate30)) {
                                try {
                                    DateFormat df = new SimpleDateFormat("HH:mm:ss");
                                    String reportDate = df.format(taskDate);
                                    notiText += "It's almost " + reportDate + "! ";
                                } catch (Exception e) {
                                    notiText += "It's almost time! ";
                                }
                            } else {
                                continue;
                            }
                        }

                        if (task.getLocation() != null && !task.getLocation().equals("")) {

                            String[] locations = task.getLocation().split("''");
                            for (int j = 0; j < locations.length; j++) {
                                String locationStr = locations[j];

                                if (locationStr.length() > 0) {
                                    String[] locationArr = locationStr.split(",,");

                                    String name = locationArr[0];
                                    double latitude = Double.parseDouble(locationArr[1]);
                                    double longitude = Double.parseDouble(locationArr[2]);

                                    float distance = distance(location.getLatitude(), location.getLongitude(), latitude, longitude);

                                    if (distance < (LOCATION_DISTANCE)) {
                                        notiText += name + " is nearby! ";
                                        break;
                                    } else {
                                        continue;
                                    }

                                }
                            }
                        }

                        if (task.getWeather() != null && !task.getWeather().equals("")) {
                            if (!weather_description_high.equals("")) {

                                String taskWeather = task.getWeather();

                                String weather_description = weather_description_high;
                                String processed_weather = "";

                                if (weather_description.contains("rain")) {
                                    processed_weather = "Rainy";
                                } else if (weather_description.contains("cloud")) {
                                    processed_weather = "Cloudy";
                                } else {
                                    processed_weather = "Clear";
                                }

                                if (taskWeather.equals(processed_weather)) {
                                    notiText += "The weather is " + taskWeather + "! ";

                                    // build notification
                                    sendNotification(notiTitle, notiText);
                                }
                            }
                        } else {
                            if (!notiText.equals("")) sendNotification(notiTitle, notiText);
                        }

                    }

//                } catch (Exception e) {
//                    Log.e(TAG, "error: "+e.getLocalizedMessage());
//                }
            }
        }, 0, INTERVAL); // repeat based on interval
    }


    /** ========================= send notification method ========================= */

    private void sendNotification(String title, String text) {
        // intent to launch on notification click
        Intent notificationHandlerIntent = new Intent(getApplicationContext(), MainActivity.class);
        notificationHandlerIntent.putExtra("key", "value");
        PendingIntent pending = PendingIntent.getActivity(getApplicationContext(), 0, notificationHandlerIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        // generate hash for notificationId
        int notificationId = 0;
        try {
            MessageDigest objSHA1 = MessageDigest.getInstance("SHA-1");
            byte[] bytSHA1 = objSHA1.digest(title.getBytes());
            BigInteger intNumSHA1 = new BigInteger(1, bytSHA1);
            notificationId = intNumSHA1.intValue() - 100000;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        Notification notification = noti
                .setContentTitle(title)
                .setContentText(text)
                .setContentIntent(pending)
                .build();

        notificationManager.notify(getResources().getString(R.string.app_name), notificationId, notification);

    }


    /** ========================= location handling methods ========================= */

    private class LocationListener implements android.location.LocationListener {

        // update current location
        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            currentLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            currentLocation = location;
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) { }

        @Override
        public void onProviderEnabled(String s) { }

        @Override
        public void onProviderDisabled(String s) { }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager - LOCATION_INTERVAL: " + INTERVAL + " LOCATION_DISTANCE: " + LOCATION_DISTANCE);
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if (isNetworkEnabled) {
            mLocationListeners = new BackgroundNotifications.LocationListener[]{
                    new BackgroundNotifications.LocationListener(LocationManager.NETWORK_PROVIDER)
            };

            currentLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        if (currentLocation == null && isGPSEnabled) {
            mLocationListeners = new BackgroundNotifications.LocationListener[]{
                    new BackgroundNotifications.LocationListener(LocationManager.GPS_PROVIDER)
            };
        }

        if (currentLocation == null) { Log.e(TAG, "get location failed");}

    }

    // calculates the distance between two locations in m
    public static float distance(double lat1, double lng1, double lat2, double lng2) {

        Location loc1 = new Location("");
        loc1.setLatitude(lat1);
        loc1.setLongitude(lng1);

        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(lng2);

        float distanceInMeters = loc1.distanceTo(loc2);

        return distanceInMeters;

    }
}
