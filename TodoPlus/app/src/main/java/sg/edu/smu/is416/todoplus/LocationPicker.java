package sg.edu.smu.is416.todoplus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import java.util.ArrayList;


public class LocationPicker extends AppCompatActivity {
    protected static int PLACE_PICKER_REQUEST = 123;
//    protected Place selectedPlace;
    private ArrayList<Place> selectedPlaces;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_picker);

        selectedPlaces = new ArrayList<>();

        // for select using autocomplete
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) { addPlaceToList(place); }

            @Override
            public void onError(Status status) {
                Toast.makeText(LocationPicker.this, "An error occurred: " + status, Toast.LENGTH_LONG).show();
            }
        });
    }

    // select on map
    protected void startPicker(View view) throws GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        if (selectedPlaces.size() < 3) {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } else {
            Toast.makeText(this, "max 3 places", Toast.LENGTH_SHORT).show();
        }
    }

    // select on map result
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                addPlaceToList(place);

                PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
                autocompleteFragment.setText(place.getName());
            }
        }
    }

    // confirm and send back to form
    public void confirmLocation(View view) {
        Intent goBack = new Intent();

        if (selectedPlaces.size() > 0) {
            StringBuilder places = new StringBuilder();
            StringBuilder lats = new StringBuilder();
            StringBuilder lngs = new StringBuilder();

            for (Place place : selectedPlaces) {
                places.append(place.getName().toString() + ",");
                lats.append(Double.toString(place.getLatLng().latitude) + ",");
                lngs.append(Double.toString(place.getLatLng().longitude) + ",");
            }

            // remove tailing comma
            String locationNames = places.toString().replaceAll(",$", "");
            String locationLats = lats.toString().replaceAll(",$", "");
            String locationLngs = lngs.toString().replaceAll(",$", "");

            goBack.putExtra("locationNames", locationNames);
            goBack.putExtra("locationLats", locationLats);
            goBack.putExtra("locationLngs", locationLngs);
        }

        setResult(RESULT_OK,goBack);
        finish();
    }

    // display currently selected places
    private void refreshSelectedPlaces() {
        ArrayList<String> placeNames = new ArrayList<>();
        for (Place place : selectedPlaces) {
            placeNames.add(place.getName().toString()+"\n"+place.getAddress().toString()+"\n");
        }


        ListView lv = (ListView) findViewById(R.id.selected_places);
        ArrayAdapter adapter = new ArrayAdapter<>(
                this,//context
                R.layout.selected_place_item,//layout
                R.id.task_title,
                placeNames//items
        );
        lv.setAdapter(adapter);
    }

    public void addPlaceToList(Place place) {
        if (selectedPlaces.size() < 3) {
            selectedPlaces.add(place);
            refreshSelectedPlaces();
        } else {
            Toast.makeText(this, "max 3 places", Toast.LENGTH_SHORT).show();
        }
    }

    public void removePlace(View view) {
        //get the row the clicked button is in
        LinearLayout vwParentRow = (LinearLayout) view.getParent();
        String rawString = ((TextView) vwParentRow.getChildAt(0)).getText().toString();

        String placeToRemove = rawString.split("\n")[0];

        for (Place place : selectedPlaces) {
            if (place.getName().toString().equals(placeToRemove)) {
                selectedPlaces.remove(place);
                break;
            }
        }
        refreshSelectedPlaces();
    }

}
