package sg.edu.smu.is416.todoplus;

import java.util.Date;

/**
 * Created by jieshiaw on 19/3/18.
 */

public class Task {
    private String name;
    private Date time;
    private String location;
    private String weather;

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, Date time) {
        this.name = name;
        this.time = time;
    }

    public Task(String name, Date time, String location, String weather) {
        this.name = name;
        this.time = time;
        this.location = location;
        this.weather = weather;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", time='" + time + '\'' +
                ", location='" + location + '\'' +
                ", weather='" + weather + '\'' +
                '}';
    }
}
