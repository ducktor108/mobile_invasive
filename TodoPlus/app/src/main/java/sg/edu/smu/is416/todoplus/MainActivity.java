package sg.edu.smu.is416.todoplus;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import sg.edu.smu.is416.todoplus.db.TaskContract;
import sg.edu.smu.is416.todoplus.db.TaskDbHelper;

public class MainActivity extends AppCompatActivity {
    private static int REQ_CODE_LOCATION_PICKER = 5566;
    private static int REQ_CODE_DETAILS_PICKER = 1234;
    private static int unlock = 0;

    String task = "";

    private static final String TAG = "MainActivity";
    public static TaskDbHelper mHelper;
    private ListView mTaskListView;
    private ArrayAdapter<String> mAdapter;


    private void updateUI() {
        ArrayList<String> taskList = new ArrayList<>();

        // retrieve from db
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = db.query(TaskContract.TaskEntry.TABLE,
                new String[]{TaskContract.TaskEntry._ID, TaskContract.TaskEntry.COL_TASK_TITLE},
                null, null, null, null, null);

        // move values to taskList
        while (cursor.moveToNext()) {
            int idx = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_TITLE);
            taskList.add(cursor.getString(idx));
        }

        if (mAdapter == null) {
            mAdapter = new ArrayAdapter<>(this,
                    R.layout.item_todo,
                    R.id.task_title,
                    taskList);
            mTaskListView.setAdapter(mAdapter);
        } else {
            mAdapter.clear();
            mAdapter.addAll(taskList);
            mAdapter.notifyDataSetChanged();
        }

        cursor.close();
        db.close();
    }

    /** ========================= onCreate method ========================= */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.sqliteLayout).setVisibility(View.GONE);

        // start background service
        startService(new Intent(this, BackgroundNotifications.class));

        mHelper = new TaskDbHelper(this);
        mTaskListView = (ListView) findViewById(R.id.list_todo);

        updateUI();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /** ========================= handle tasks ========================= */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_task:
                final EditText taskEditText = new EditText(this);
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Add a new task")
                        .setMessage("What do you want to do next?")
                        .setView(taskEditText)
                        .setNeutralButton("Set Reminder", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                task = String.valueOf(taskEditText.getText());
                                task.replaceAll("[^a-zA-Z0-9 ]","");

                                launchDetailsActivity(task);
                            }
                        })
                        .setPositiveButton("Add Task", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                task = String.valueOf(taskEditText.getText());
                                task.replaceAll("[^a-zA-Z0-9 ]","");

//                                String temp = task.toLowerCase().trim();
//                                if (temp.equals("location") || temp.equals("weather") || temp.equals("time")) {
                                SQLiteDatabase db = mHelper.getWritableDatabase();

                                // remove old task (if it exists)
                                String whereClause = TaskContract.TaskEntry.COL_TASK_TITLE + " = '" + task + "'";
                                db.delete(TaskContract.TaskEntry.TABLE, whereClause, null);

                                ContentValues values = new ContentValues();
                                values.put(TaskContract.TaskEntry.COL_TASK_TITLE, task);
                                db.insertWithOnConflict(TaskContract.TaskEntry.TABLE,
                                        null,
                                        values,
                                        SQLiteDatabase.CONFLICT_REPLACE);
                                db.close();
                                updateUI();
//                                } else {
//                                    Toast.makeText(getApplicationContext(), "error: key word not allowed", Toast.LENGTH_SHORT).show();
//                                }

                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create();

                dialog.show();
                return true;
            case R.id.secret:
                unlock++;
                if (unlock == 7) findViewById(R.id.sqliteLayout).setVisibility(View.VISIBLE);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void deleteTask(View view) {
        View parent = (View) view.getParent();
        TextView taskTextView = (TextView) parent.findViewById(R.id.task_title);
        String task = String.valueOf(taskTextView.getText());
        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.delete(TaskContract.TaskEntry.TABLE,
                TaskContract.TaskEntry.COL_TASK_TITLE + " = ?",
                new String[]{task});
        db.close();
        updateUI();
    }

    /** ========================= intent and activity methods ========================= */

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_CODE_LOCATION_PICKER) {
            if (resultCode == RESULT_OK && data.getExtras() != null && data.getExtras().containsKey("locationNames")) {
                String[] locationNames = data.getStringExtra("locationNames").split(",");
                String[] locationLats = data.getStringExtra("locationLats").split(",");
                String[] locationLngs = data.getStringExtra("locationLngs").split(",");

                for (int i = 0; i < locationNames.length; i++) {
                    Toast.makeText(this, locationNames[i]+": "+locationLats[i]+
                            ", "+locationLngs[i],Toast.LENGTH_SHORT).show();
                }

            }
        }

        updateUI();
    }

    public void editTask(View view) {
        //get the row the clicked button is in
        LinearLayout vwParentRow = (LinearLayout) view.getParent();
        String taskToEdit = ((TextView) vwParentRow.getChildAt(0)).getText().toString();

        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("taskName", taskToEdit);
        intent.putExtra("function", "edit");
        startActivityForResult(intent, REQ_CODE_DETAILS_PICKER);
    }

    public void launchDetailsActivity(String taskName) {
        //launch the add task activity and wait for a result
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("taskName", taskName);
        intent.putExtra("function", "add");
        startActivityForResult(intent, REQ_CODE_DETAILS_PICKER);
    }

    public void accessSQLite(View view) {
        Intent dbmanager = new Intent(this, AndroidDatabaseManager.class);
        startActivity(dbmanager);
    }
}